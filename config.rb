# --------------------------------------
#   Config
# --------------------------------------

# ----- Middleman ----- #

activate :livereload

# ----- Assets ----- #

set :css_dir, 'assets/stylesheets'
set :images_dir, 'assets/images'
# set :fonts_dir, 'assets/fonts'

# ----- Images ----- #

activate :automatic_image_sizes

# ----- Directories for assets ----- #

activate :directory_indexes

# --------------------------------------
#   Production
# --------------------------------------

# ----- Prefixing ----- #

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
  config.cascade = false
end

# ----- Build ----- #

# ignore 'assets/fonts/*'

configure :build do
  activate :minify_css
  activate :minify_javascript
  activate :relative_assets
  activate :gzip
  activate :move
end

# Moves required Typography.com directory untouched

# class Move < Middleman::Extension
#   def after_build
#     FileUtils.cp_r('source/assets/fonts/', 'build/assets')
#   end
# end

# ::Middleman::Extensions.register(:move, Move)
